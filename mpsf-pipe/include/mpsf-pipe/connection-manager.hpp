/** @file
modular-protocol-system-framework
Copyright (C) 2020  sapient_cogbag <sapient_cogbag at protonmail dot com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include <mpsf-util/typedefs.hpp>
#include <string>

/**
 * Holds information on how to connect CSPs together nyaa
 */
namespace mpsf::connection {    
    using namespace mpsf::util::typedefs;

    /**
     * Controls the formation and destruction of connections programmatically.
     */
    template <typename Channel>
    struct ConnectionNexus {
        

    };


}
