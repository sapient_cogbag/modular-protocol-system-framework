/** @file
modular-protocol-system-framework
Copyright (C) 2020  sapient_cogbag <sapient_cogbag at protonmail dot com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include <mpsf-util/typedefs.hpp>
#include <string>
#include <variant>
#include <functional>

namespace mpsf::util::error {


   /**
    * Holds an error message (u8string)
    */
    struct error_message {
        std::u8string message;
    };
    
    /**
     * Holds a result or error (at least until c++ gets decent exceptions nyaa)
     *
     * Uses @class error_message if you want to provide an error message nya
     * (in particular, error_message {u8"..."s})
     */
    template <typename T>
    struct maybe_error : std::variant<T, error_message> {
        /**
         * Get the value unsafely.
         */
        const T& value_unsafe() const {
            return std::get<T>(*this);
        }

        /**
         * Get the value unsafely.
         */
        T& value_unsafe() {
            return std::get<T>(*this);
        }

        /**
         * Check if this potential error is an error or value
         */
        bool is_error() const {
            return std::holds_alternative<error_message>(*this);
        };
        
        /**
         * Get the error message in an unsafe manner.
         */
        const error_message& error_message_unsafe() const {
            return std::get<error_message>(*this); 
        }

        
    };

    /**
     *  Call if there is not an error. nya
     */
    template <typename P, typename Callable>
    inline auto call_not_error(Callable a, maybe_error<P> val) 
        -> maybe_error<std::invoke_result_t<Callable, P>> {
        using invoked_result_t = std::invoke_result_t<Callable, P>;

        if (val.is_errored()) {
            return error_message{val.error_message_unsafe()};
        } else {
            return std::invoke(a, val.value_unsafe());
        } 
    }
}
