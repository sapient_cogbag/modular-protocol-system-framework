/** @file
modular-protocol-system-framework
Copyright (C) 2020  sapient_cogbag <sapient_cogbag at protonmail dot com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include <mpsf-util/typedefs.hpp>
#include <concepts>
#include <variant>
#include <tuple>
#include <mpsf-util/concepts.hpp>

namespace mpsf::util::alg {
    using namespace mpsf::util::typedefs;

    /**
     * Provides for numeric type algebras by providing a struct to inherit from
     * with sensible operators.
     *
     * Uses CRTP to require type-coherency.
     *
     * The internal data is stored in .data nya.
     */
    template <typename T, typename Derived>
    struct numerical_type {
         T data;
         // probably for compiler detection reasons, we have to use
         // the template for the argument not just the given Derived.
         auto operator <=>(const numerical_type& a) const = default;
    };

    template <typename... T>
    struct one_of : std::variant <T...> {};

    template <typename... T>
    struct all_of : std::tuple <T...> {};
}


namespace mpsf::util::typedefs {
    namespace alg {
        using namespace mpsf::util::alg;
    }
}
