/** @file
modular-protocol-system-framework
Copyright (C) 2020  sapient_cogbag <sapient_cogbag at protonmail dot com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

This file contains implementations of some common concepts from https://en.cppreference.com/w/cpp/named_req/
nyaa, as well as potentially miscellaneous other concepts ^.^
*/

#pragma once
#include <mpsf-util/typedefs.hpp>
#include <concepts>
#include <type_traits>
#include <variant>



namespace mpsf::util {
    using namespace typedefs;

    /**
     * Tests if the tested type is the same as any of the other listed types nya
     *
     * Usage: `auto `
     */
    template <typename TestedType, typename... Ts>
    concept one_of = (std::same_as<TestedType, Ts> ||...); 

    /**
     * Tests if the tested type is assignable to one of the given types nya.
     */
    template <typename TestedType, typename... Ts>
    concept assignable_to_one_of = (std::assignable_from <Ts, TestedType>||...);

    // {{{ Standard-ish concepts ;3 https://en.cppreference.com/w/cpp/named_req
    
    /**
     * https://en.cppreference.com/w/cpp/named_req/Erasable
     * Note the default things w.r.t the allocator nya
     *
     * Also note that there is no way to verify the post-conditions.
     */
    template <typename T, typename Allocator = std::allocator<T>>
    concept erasable = requires (Allocator m, T* ptr) {
        std::allocator_traits <Allocator>::destroy(m, ptr);
    };

    /**
     * https://en.cppreference.com/w/cpp/named_req/MoveInsertable
     *
     * Again note the default things with the allocator type nya
     *
     * Also note that there is no real way to verify the post-conditions.
     */
    template <typename T, typename Allocator = std::allocator<T>>
    concept move_insertable = requires (T* ptr, T&& rvalue, Allocator alloc) {
        std::allocator_traits <Allocator>::construct(alloc, ptr, rvalue); 
    };
    
    /**
     * https://en.cppreference.com/w/cpp/named_req/CopyInsertable
     *
     * Note the allocator defaults (as per spec)
     *
     * Also note that there is no real way to verify the post-conditions nya.
     */
    template <typename T, typename Allocator = std::allocator<T>>
    concept copy_insertable = requires {
        requires requires (T* ptr, const T t, Allocator alloc) { 
            std::allocator_traits <Allocator>::construct(alloc, ptr, t);
        } || requires (T* ptr, T t, Allocator alloc) { 
            std::allocator_traits <Allocator>::construct(alloc, ptr, t);
        };
    };

    /**
     * https://en.cppreference.com/w/cpp/named_req/Destructible
     * Destructability. Note that the special/running conditions aren't really verifiable
     * (in part due to unreliability of `noexcept` properties nya.
     */
    template <typename T>
    concept destructible = std::is_destructible_v<T>; 
    
             
    // {{{MANY OF THE CONCEPTS ARE PROVIDED BY #include <concepts>

    /**
     * https://en.cppreference.com/w/cpp/named_req/DefaultConstructible
     */
    //template <typename T>
    //concept DefaultConstructible = std::is_default_constructible_v <T>;

    /**
     * https://en.cppreference.com/w/cpp/named_req/CopyConstructible
     */
    //template <typename T>
    //concept CopyConstructible = std::is_copy_constructible_v <T>;

    /**
     * https://en.cppreference.com/w/cpp/named_req/EqualityComparable
     *
     * Implicit conversion means we do some funkiness with if statements
     */
    //template <typename T>
    //concept EqualityComparable = requires (T a, T b) {
    //    { a == b }
    //}; 
    //}}}
    
    /**
     * https://en.cppreference.com/w/cpp/named_req/Container
     *
     * TODO: Switch the iterators to use Legacy[...]Iterator based concepts.
     */
    template <typename T>
    concept container = requires (T a, T b, T&& mov)  {
        typename T::value_type;
        typename T::reference;
        typename T::iterator;
        typename T::const_iterator;
        requires requires (typename T::iterator it) {*it;};
        requires requires (typename T::const_iterator it) {*it;};
        // Have to do some funkery for nyaaa where there are containers with and
        // without allocator_type (which we need to test with Eraseable)
        erasable<typename T::value_type> || 
            erasable<typename T::value_type, typename T::allocator_type>;
        
        std::same_as<
            typename T::difference_type, 
            typename std::iterator_traits <typename T::iterator>::difference_type
        >;
        std::same_as<
            typename T::difference_type,
            typename std::iterator_traits <typename T::const_iterator>::difference_type
        >;
        typename T::size_type;

        T();
        T(a);
        T(mov);
        { a = b } -> std::same_as <T&>;
        { a = mov } -> std::same_as <T&>;
        { a.~T() } -> std::same_as <void>;
        { a.begin() } -> one_of <typename T::iterator, typename T::const_iterator>;
        { a.end() } -> one_of <typename T::iterator, typename T::const_iterator>;
        { a.cbegin() } -> std::same_as <typename T::const_iterator>;
        { a.cend() } -> std::same_as <typename T::const_iterator>;
        { a == b } -> std::convertible_to <bool>;
        { a != b } -> std::convertible_to <bool>;
        { a.swap(b) } -> std::same_as <void>;
        { swap(a, b) } -> std::same_as <void>;
        { a.size() } -> std::same_as <typename T::size_type>;
        { a.max_size() } -> std::same_as <typename T::size_type>;
        { a.empty() } -> std::convertible_to <bool>;

        // Some general things to ensure we meet good requirements nya
        std::default_initializable <T>;
        std::copy_constructible <T>;
        std::equality_comparable <T>;
        std::swappable <T>;

        // value type requirements
        copy_insertable <typename T::value_type> || 
            copy_insertable <typename T::value_type, typename T::allocator_type>;
        std::equality_comparable <typename T::value_type>;
        destructible <typename T::value_type>; 
    }; 

    /**
     * Tests for a container of a specific type
     */
    template <typename container_test_t, typename of_type> 
    concept container_of = requires {
        container <container_test_t>;
        std::same_as <typename container_test_t::value_type, of_type>;
    };

    /**
     * Container of elements convertible-to a given type nya
     */
    template <typename container_test_t, typename convertible_to_t>
    concept container_of_convertible_to = requires {
        container <container_test_t>;
        std::convertible_to <typename container_test_t::value_type, convertible_to_t>;
    };
    // }}}
    

    namespace _concept_impl {
        /**
         * Test if a type is an instantiation of the given template nya.
         */
        template <template <typename...> typename Template>
        struct template_query_t {
            template <typename T>
            struct instantiates_to : std::false_type {};

            template <typename... Q>
            struct instantiates_to <Template <Q...>> : std::true_type {}; 
        };

        /**
         * We cannot use lambdas in requirements because they are unevaluated operands nya
         * and lambdas are not allowed there as per: https://stackoverflow.com/a/22232165
         *
         * So we use this as a neat lambda object nyaa
         * that takes arbitrary forwarding arguments and does nothing :3
         */
        inline constexpr auto plain_lambda = [](auto&&... args){};

    }
    

    template <typename T>
    concept pointer = std::is_pointer_v<T>;


    /**
     * Test if a variant type has a custom variant size getter
     * at Variant::variant_size_v
     */
    template <typename Variant>
    concept has_custom_variant_size_v = requires {
        { Variant::variant_size_v } -> std::same_as <usizem>;
    };


    /**
     * If this has the standard variant size accessor (std::variant_size_v)
     *
     * Overridden by Variant::variant_size_v if it exists nya
     */
    template <typename Variant>
    concept has_std_variant_size_v = requires {
        !has_custom_variant_size_v <Variant>;
        { std::variant_size_v <Variant> } -> std::same_as <usizem>;
    };


    /**
     * Test if a variant type has a custom alternative-from-index
     * type getter nya (at Variant::variant_alternative_t <usize>)
     */
    template <typename Variant>
    concept has_custom_variant_alternative_t = requires {
        typename Variant::template variant_alternative_t<0>;
    };

    /**
     * Test if a variant type works with a standard
     * std::variant_alternative_t <index, Variant> nyaa
     *
     * Is overridden by has_custom_variant_alternative_t
     */
    template <typename Variant>
    concept has_std_variant_alternative_t = requires {
        !has_custom_variant_alternative_t <Variant>;
        typename std::variant_alternative_t <0, Variant>;
    };




    /**
     * Specifies a variant-style type.
     *
     * You can use your own types via having stuff such as ::variant_size_v
     * and ::variant_alternative_t 
     *
     * ADL is used for the functions on the variants. C++20 
     * fixes the problem discussed in 
     * https://stackoverflow.com/questions/16173902/enable-stdget-support-on-class#comment99497515_16174781
     *
     * nyaaa
     *
     * we also use our special lambda object :)
     */ 
    template <typename Variant>
    concept varying = requires (Variant mut_var, const Variant var) {
        { var.index() } -> std::same_as<usizem>;
        has_custom_variant_size_v <Variant> || 
            has_std_variant_size_v <Variant>;
        has_custom_variant_alternative_t <Variant> || 
            has_std_variant_alternative_t <Variant>;
        get<0>(mut_var);
        get<0>(var);
        { get_if<0>(mut_var) } -> pointer;
        { get_if<0>(var) } -> pointer;
        visit(mpsf::util::_concept_impl::plain_lambda, mut_var);
        visit(mpsf::util::_concept_impl::plain_lambda, var); 
    };

    /**{{{
     * Processes for manipulating `varying` types nya 
     */
    namespace _impl {
        template <typename Varying>
        struct varying_size{};

        template <typename Varying>
        requires has_std_variant_size_v <Varying>
        struct varying_size <Varying> {
            static constexpr usizem value = std::variant_size_v <Varying>;    
        };

        template <typename Varying>
        requires has_custom_variant_size_v <Varying>
        struct varying_size <Varying> {
            static constexpr usizem value = Varying::variant_size_v;
        };

        template <typename Varying>
        struct varying_alternative {};

        template <typename Varying>
        requires has_custom_variant_alternative_t <Varying>
        struct varying_alternative <Varying> {
            template <usizem ID>
            using type = typename Varying::template variant_alternative_t<ID>;
        };

        template <typename Varying>
        requires has_std_variant_alternative_t <Varying>
        struct varying_alternative <Varying> {
            template <usizem ID>
            using type = std::variant_alternative_t <ID, Varying>;
        };
    }

    template <typename Varying> requires varying <Varying>
    inline constexpr usizem varying_size_v = _impl::varying_size <Varying>::value;
    
    template <typename Varying, usizem ID> requires varying <Varying>
    using varying_alternative_t = typename _impl::varying_alternative <Varying>::template type <ID>;

    namespace _impl {
        //// some template metaprogramming for seeeing if something is 
        //in a varying nyaaa
        
        template <usizem ID, typename Varying, typename T>
        requires varying<Varying>
        struct varying_id_is_t {
            static constexpr bool value = std::same_as <
                T, varying_alternative_t<Varying, ID>
            > || varying_id_is_t <ID - 1, Varying, T>::value;
        };

        template <typename Varying, typename T>
        struct varying_id_is_t <0, Varying, T> {
            static constexpr bool value  =std::same_as <
                T, varying_alternative_t <Varying, 0>
            >;
        };
    }


    /**
     * Check if a type can be held in a variant-derived type, or at least one
     * which has https://en.cppreference.com/w/cpp/utility/variant -like properties nya
     *
     * In particular it should be a `varying` concept nya
     *  
     */
    template <typename T, typename Varying>
    concept can_hold_in_varying = _impl::varying_id_is_t <
        varying_size_v<Varying>, Varying, T
    >::value; 

    //}}}

    /**
     * Check if a type is an instantiation of a template nya.
     */
    template <typename T, template <typename... > typename Template>
    concept instantiation_of = requires {
        _concept_impl::template template_query_t <Template>::template instantiates_to <T>::value;
    };



}
