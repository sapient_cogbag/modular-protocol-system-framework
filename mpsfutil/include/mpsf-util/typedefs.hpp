/** @file
modular-protocol-system-framework
Copyright (C) 2020  sapient_cogbag <sapient_cogbag at protonmail dot com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include <cstdint>
#include <cstddef>
#include <algorithm>
#include <string>

/**
 * Provides useful abbreviations like u8c/u32m/i8c/etc and other such things 
 */
namespace mpsf::util::typedefs {
    // {{{ Mutable int abbreviations
    using u8m = std::uint8_t;
    using u16m = std::uint16_t;
    using u32m = std::uint32_t;
    using u64m = std::uint64_t;
    
    using i8m = std::int8_t;
    using i16m = std::int16_t;
    using i32m = std::int32_t;
    using i64m = std::int64_t;

    using usizem = std::size_t;
    // }}}
   

    // {{{ immutable int abbreviations
    using u8c = const std::uint8_t;
    using u16c = const std::uint16_t;
    using u32c = const std::uint32_t;
    using u64c = const std::uint64_t;
    
    using i8c = const std::int8_t;
    using i16c = const std::int16_t;
    using i32c = const std::int32_t;
    using i64c = const std::int64_t;

    using usizec = const std::size_t;
    // }}}
   

    /**
     * Provides a pointer indicating non-ownership nya
     */ 
    template <typename T>
    struct observer_ptr {
        private:
        T* v;

        public:
        T* const operator*() {
             return v;   
        }

        T* const operator->() {
            return v;
        }

        const T* const operator*() const {
             return v;   
        }

        const T* const operator->() const {
            return v;
        }

        // {{{ COMPARISON AND ARITHMETIC OPERATORS
        bool operator==(observer_ptr const other) const {
            return other.v == this->v;
        }

        bool operator==(T* const other) const {
            return v == other;
        }

        bool operator==(std::nullptr_t) const {
            return v == nullptr;
        }

        auto operator<=>(observer_ptr const other) const {
            return v <=> other.v;
        }
        
        auto operator+(auto&& a) const {
            return observer_ptr(v + std::forward<decltype(a)>(a));
        }

        auto operator-(auto&& a) const {
            return observer_ptr(v - std::forward<decltype(a)>(a));
        }

        // prefix
        auto& operator++() {
            *this = *this + 1;
            return *this;
        }

        // https://en.cppreference.com/w/cpp/language/operators
        auto operator++(int) -> observer_ptr {  // postfix
            const auto old = *this;
            ++(*this);
            return old;

        }
    
        // prefix
        auto& operator--() {
            *this = *this - 1;
            return *this;
        }

        // https://en.cppreference.com/w/cpp/language/operators
        auto operator--(int) -> observer_ptr {  // postfix
            const auto old = *this;
            --(*this);
            return old;

        }

        // }}} 
        
        /**
         * Get the raw pointer inside the observer pointer
         */ 
        T* const get_raw_ptr() const {
            return v;
        }

    };

    using namespace std::string_literals;

} 
