/** @file
modular-protocol-system-framework
Copyright (C) 2020  sapient_cogbag <sapient_cogbag at protonmail dot com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include <mpsf-util/typedefs.hpp>
#include <mpsf-util/concepts.hpp>
#include <mpsf-util/error.hpp>
#include <mpsf-util/algebraic-types.hpp>
#include <coroutine>
#include <functional>
#include <variant>
#include <queue>
#include <ranges>
#include <vector>
#include <array>
#include <concepts>

namespace mpsf::proto {
    using namespace mpsf::util::typedefs;
    struct message_type_id_t : alg::numerical_type <u32m, message_type_id_t> {};
    struct transmission_message_id_t : message_type_id_t {};
    struct reception_message_id_t : message_type_id_t {};

    /**
     * Holds types to specify the result states of attempting to decode the id of chunked
     * protocol nya.
     *
     * {{{
     */
    namespace chunked_id_decode_result {
        /**
         * A message id was read, but it was invalid nya.
         */
        struct invalid_message_id {
            std::size_t consumed_protocol_chunks;
        };
        /**
         * No message id was found at all.
         */
        struct no_message_id {};

        /**
         * A valid message id was found
         */
        struct valid_message_id {
            reception_message_id_t id;
            std::size_t consumed_protocol_chunks;
        };

        struct result_t : alg::one_of <
            invalid_message_id, 
            no_message_id, 
            valid_message_id
        > {};

       static_assert(mpsf::util::can_hold_in_variant<valid_message_id, result_t>, "Must be able to use on derivative types"); 
    }


    // }}}
     
    /**
     * Holds an implementation of ways for translating to a system using
     * a given base chunk (e.g. characters, objects in the case of using queues, etc.) nyaa
     *
     * This is transmission only and a synchronous system.
     *
     * @tparam protocol_transmission_chunk The unit chunks of the protocol output to 
     *  use. e.g. char, u8m, etc.
     *
     * @tparam trans_protocol_message_object A single type of object the implementation will 
     *  be sending over the network nya - chances is this is a `mpsf::util::alg::one_of` 
     *  object (this is just a derived class for std::variant) of the actual messages 
     *  you care about nya.
     *
     * Note that we cannot check compatibility with arbitrary iterables. We are trusting 
     * you to accurately do this (with some tests for a couple standard iterables).
     *
     * STATIC METHODS:
     *  encode_id takes a message identifier nya, and converts it into some arbitrary
     *      container (TODO: Make into range)
     *  encode_main_message takes a protocol message and encodes it into a sequence of 
     *      protocol chunks (TODO: Make into range)
     *
     */
    template <
        typename T, 
        typename protocol_transmission_chunk, 
        typename trans_protocol_message_object
    >
    concept chunked_protocol_transmitter_sync = requires (
            transmission_message_id_t id, 
            const trans_protocol_message_object& msg,
            const std::vector <protocol_transmission_chunk>& itercheck_1,
            const std::vector <protocol_transmission_chunk>& itercheck_2
        ) {
        { T::encode_id(id) } -> mpsf::util::container_of_convertible_to <protocol_transmission_chunk>;
        { T::encode_main_message(msg) } -> mpsf::util::container_of_convertible_to <protocol_transmission_chunk>;
    };
    



    /**
     * Holds an implementation of a method of taking a chunked protocol (e.g. it
     * transmits chunks of bytes/chars/etc. nya) and converting it to the given
     * message object.
     *
     * @tparam protocol_reception_chunk is the units that the protocol transmits
     *  in, e.g. char, u8m, etc.
     *
     * @tparam recv_protocol_message_object is the types of message that you want
     *  to receive.
     *
     * This is reception only and a synchronous translation system. 
     *
     * Note that we cannot check compatibility with arbitrary iterables. We are trusting 
     * you to accurately do this (with some tests for a couple standard iterables).
     * 
     * STATIC METHODS: 
     *  decode_id takes a pair of iterators and scans for an identifier nyaaa. It 
     *      should return any of the types (or more than one via result_t) in 
     *      chunked_id_decode_result. 
     */
    template <
        typename T,
        typename protocol_reception_chunk,
        typename recv_protocol_message_object
    >
    concept chunked_protocol_receiver_sync = requires (
            reception_message_id_t id,
            const recv_protocol_message_object& msg, 
            const std::vector <protocol_reception_chunk>& itercheck_1,
            const std::vector <protocol_reception_chunk>& itercheck_2
        ){ 
        // test we can turn result into result_t
        chunked_id_decode_result::result_t {T::decode_id(itercheck_1.cbegin(), itercheck_1.cend())};
        chunked_id_decode_result::result_t {T::decode_id(itercheck_2.cbegin(), itercheck_2.cend())};
    };


    /**
     * Holds a set of messages for a protocol, and methods of processing that protocol.
     */
    template <typename protocol_translator_system, typename... message_types>
    struct protocol_message_collection {
        using protocol_message = alg::one_of <message_types...>; 
    };

}
