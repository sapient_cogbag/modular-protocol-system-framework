# modular-protocol-system-framework
# Copyright (C) 2020  sapient_cogbag <sapient_cogbag at protonmail dot com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

cmake_minimum_required(VERSION 3.16)
project(mpsf)

add_subdirectory(mpsfutil)
add_subdirectory(mpsf-proto)
target_link_libraries(mpsf-proto mpsf-util)
add_subdirectory(mpsf-pipe)
target_link_libraries(mpsf-pipe mpsf-proto mpsf-util)

# Enable the generation of compile_commands.json nya
set(CMAKE_EXPORT_COMPILE_COMMANDS YES)

